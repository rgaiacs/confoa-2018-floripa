# Python auxiliando na promocão do acesso aberto na Universidade Federal de Santa Catarina

## Resumo da Proposta (cerca de 200 palavras)

## Tipo de Trabalho

- Pecha Kucha (apresentação curta)

## Tema da Conferência

- Ciência Aberta e outras expressões de conhecimento aberto

  - Desenvolvimento e comunidades de software livre para a promoção do acesso aberto e da ciência aberta

  - Outras práticas de conhecimento aberto (hardware e software livre, educação aberta)

## Palavras-chave

Ciência Aberta, Python, Oceanografia

## Audiência

Esse trabalho destina-se à  bibliotecários, programadores, decisores políticos, profissionais de comunicação de ciência, gestores de tecnologias de informação.

## Proposta (até um máximo de 1000 palavras)

Em 2016,
organizou-se o "Workshop de Python para Oceanógrafos e Biólogos"
em Florianópolis, Brasil.
O workshop foi envisionado a partir de conversas na Conferência SciPy Latin America 2016,
também sediada em Florianópolis,
e obteve uma demanda três vezes maior que a capacidade suportada pelo laboratório disponível.

Durante o workshop, ...

Em 2017,
...

Em 2018,
...

## Referências