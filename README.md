# Pecha Kuchas para a 9ª Conferência Luso-Brasileira sobre Acesso Aberto

Website: http://confoa.rcaap.pt/2018/

## Conferência Luso-Brasileira sobre Acesso Aberto

A Conferência Luso-Brasileira sobre Acesso Aberto (ConfOA), com realização alternada entre Portugal e Brasil, tem como objetivo reunir as comunidades portuguesa e brasileira, que desenvolvem atividades de investigação, desenvolvimento, gestão de serviços e definição de políticas relacionadas com o Acesso Aberto ao conhecimento e com a Ciência Aberta, com o propósito de promover a partilha, discussão e divulgação de conhecimentos, práticas e investigação sobres estas temáticas, em todas as suas dimensões e perspetivas.

## Submissões

A submissão de trabalhos é efetuada em: http://conferencias.rcaap.pt/ onde será desenvolvida a avaliação dos trabalhos.

Deadline de submissões: 22 de abril de 2018

Comunicação de Aceitação: 15 de junho 2018

## Pecha Kuchas

Os Pecha Kucha são apresentações de 7 minutos, com não mais de 24 slides. As propostas deverão ter um mínimo de 1 página e um máximo de 2 páginas (ver e utilizar o modelo de proposta Pecha Kucha). As propostas de Pecha Kucha podem apresentar trabalhos de investigação e desenvolvimento recentes ou em conclusão, e eventualmente casos concretos e locais, mas com interesse e relevância geral.

As propostas com qualidade e relevância que não possam ser aceites como Pecha Kucha poderão ser consideradas para apresentação como Póster.

## Tema

- Ciência Aberta e outras expressões de conhecimento aberto

  - Desenvolvimento e comunidades de software livre para a promoção do acesso aberto e da ciência aberta

  - Outras práticas de conhecimento aberto (educação aberta)